#pragma once

#include <QtWidgets/QMainWindow>
#include<qmenubar.h>
#include "ui_QMyBaseUI.h"
#include"QMyBaseThread.h"

class QTDLLSHARED_EXPORT  QMyBaseUI : public QMainWindow
{
    Q_OBJECT

public:
    QMyBaseUI(QString title,QWidget *parent = Q_NULLPTR);
    ~QMyBaseUI();
    void SetVersionMsg(QString msg);
public slots:
    //打开版本信息界面
    void OpenVerisonindow();
private:
    void addMenuBar();
private:
    Ui::QMyBaseUIClass ui;
    QString m_versionMsg;//版本信息
};
