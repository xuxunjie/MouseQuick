#pragma once
#include <QThread>
#include <QReadWriteLock> 
#include <QtCore/qglobal.h>
#define QTDLL_LIBRARY
#if defined(QTDLL_LIBRARY)
#  define QTDLLSHARED_EXPORT Q_DECL_EXPORT
#else
#  define QTDLLSHARED_EXPORT Q_DECL_IMPORT
#endif

class QTDLLSHARED_EXPORT  QMyBaseThread : public QThread
{
public:
	QMyBaseThread();
	~QMyBaseThread();
	//初始化参数
	void InitData();	
	//返回暂停状态
	bool GetPauseType();
private slots:
	void ExitThread();//退出线程
	//设置是否暂停
	void SetPauseType(bool isPause);
private:
	QReadWriteLock m_exitLock;
	QReadWriteLock m_pauseLock;
	bool m_bIsExit;//是否退出线程
	bool m_bIsPause;//是否暂停线程
	
};