#pragma once

#include <QDialog>

class   QVerisonindow : public QDialog
{
    Q_OBJECT

public:
    QVerisonindow(QString msg,QWidget *parent = Q_NULLPTR);
    ~QVerisonindow();
    void initUI();
private slots:
   
private:
    
private:
    QString m_versionMsg;
};
