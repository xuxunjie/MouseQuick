/********************************************************************************
** Form generated from reading UI file 'QMyBaseUI.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QMYBASEUI_H
#define UI_QMYBASEUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QMyBaseUIClass
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QMyBaseUIClass)
    {
        if (QMyBaseUIClass->objectName().isEmpty())
            QMyBaseUIClass->setObjectName(QString::fromUtf8("QMyBaseUIClass"));
        QMyBaseUIClass->resize(610, 488);
        centralWidget = new QWidget(QMyBaseUIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QMyBaseUIClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QMyBaseUIClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 610, 23));
        QMyBaseUIClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(QMyBaseUIClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QMyBaseUIClass->setStatusBar(statusBar);

        retranslateUi(QMyBaseUIClass);

        QMetaObject::connectSlotsByName(QMyBaseUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *QMyBaseUIClass)
    {
        QMyBaseUIClass->setWindowTitle(QCoreApplication::translate("QMyBaseUIClass", "QtWidgetsApplication1", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QMyBaseUIClass: public Ui_QMyBaseUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QMYBASEUI_H
