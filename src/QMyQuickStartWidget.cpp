#include "..\include\QMyQuickStartWidget.h"
#include<QFileIconProvider>
#include<QGridLayout>
#include<QProcess>
QMyQuickStartWidget::QMyQuickStartWidget(QWidget* parent)
	: QWidget(parent)
{
	InitUI();
}

void QMyQuickStartWidget::InitUI()
{
	m_listWidget = new QListWidget(this);
	m_listWidget->setIconSize(QSize(48, 48));         // 设置单元项图片大小
	m_listWidget->setResizeMode(QListView::Adjust);   // 设置大小模式-可调节
	m_listWidget->setViewMode(QListView::IconMode);   // 设置显示模式
	m_listWidget->setMovement(QListView::Static);     // 设置单元项不可被拖动
	m_listWidget->setSpacing(10);                     // 设置单元项间距
	connect(m_listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(OpenEXE(QListWidgetItem*)));
	QGridLayout* layout = new QGridLayout();
	layout->addWidget(m_listWidget, 0, 0,1,1);
	setLayout(layout);
}

void QMyQuickStartWidget::addButtons(QStringList paths)
{
	for (int i = 0; i < paths.size(); i++)
	{
		addButton(paths[i]);
	}
}

void QMyQuickStartWidget::addButton(QString path)
{
	QFileInfo  info;
	info.setFile(path);
	QString name = info.fileName();
	// 获取图标
	QFileIconProvider provider;
	QIcon icon = provider.icon(path);
	QString strType = provider.type(path);
	// 添加单元项
	QListWidgetItem* item = new QListWidgetItem(m_listWidget);
	item->setIcon(icon);
	item->setToolTip(path);
	item->setText(name);
	m_listWidget->addItem(item);
}

void QMyQuickStartWidget::OpenEXE(QListWidgetItem* item)
{
	QString path = item->toolTip();
	QProcess::startDetached(path, QStringList());
}
