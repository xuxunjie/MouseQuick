#include "..\include\QMyTabBar.h"

QMyTabBar::QMyTabBar(QWidget* parent)
	: QWidget(parent)
{
	setMinimumWidth(60);
}


QMyTabBar::~QMyTabBar()
{
	
}

void QMyTabBar::AddTab(QString title)
{
	int index = m_titles.size();
	QMyTabBarButton* button = new QMyTabBarButton(index,title, this);
	connect(button, SIGNAL(ButtonClicked(int)), this, SLOT(SetButtonClicked(int)));
	button->setMinimumHeight(40);
	m_titles.append(button);
	repaint();
}

void QMyTabBar::resizeEvent(QResizeEvent* event)
{
	int start_x = 5;
	int start_y = 5;
	int h = 40;
	int width = this->width();
	for (int i = 0; i < m_titles.size(); i++)
	{
		m_titles[i]->setGeometry(start_x, start_y + i * h, width - start_x * 2, h);
	}
	QWidget::resizeEvent(event);
}

void QMyTabBar::SetButtonClicked(int index)
{
	if (index >= m_titles.size())
		return;
	for (int i = 0; i < m_titles.size(); i++)
	{
		if (i == index)
		{
			m_titles[i]->SetSelectType(true);
		}
		else
		{
			m_titles[i]->SetSelectType(false);
		}	
	}
	emit CurrentIndexChange(index);
}
