#include "..\include\QMyListWidget.h"
#include<QGridLayout>
#include<QFileDialog>
#include<qsettings.h>
QMyListWidget::QMyListWidget(QWidget* parent)
	: QWidget(parent)
{
	InitUI();
	ReadFile();
}

void QMyListWidget::InitUI()
{
	QGridLayout* layout = new QGridLayout();
	m_addButton = new QPushButton(u8"添加", this);
	m_addButton->setMinimumWidth(60);
	layout->addWidget(m_addButton, 0, 1, 1, 1);
	connect(m_addButton, SIGNAL(clicked()), this, SLOT(AddItem()));

	m_deleteButton = new QPushButton(u8"删除", this);
	m_deleteButton->setMinimumWidth(60);
	layout->addWidget(m_deleteButton, 1, 1, 1, 1);
	connect(m_deleteButton, SIGNAL(clicked()), this, SLOT(DeleteItem()));

	QSpacerItem* verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
	layout->addItem(verticalSpacer, 2, 1, 1, 1);

	m_saveButton = new QPushButton(u8"保存", this);
	m_saveButton->setMinimumWidth(60);
	layout->addWidget(m_saveButton, 3, 1, 1, 1);
	connect(m_saveButton, SIGNAL(clicked()), this, SLOT(SaveFile()));

	m_listWidget = new QListWidget(this);
	m_listWidget->setDragDropMode(QAbstractItemView::InternalMove);
	layout->addWidget(m_listWidget, 0, 0, 3, 1);


	setLayout(layout);
}

void QMyListWidget::AddItem()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr(u8"选中应用程序"), "", tr(u8"应用程序 (*.exe)"));
	for (int i = 0; i < m_listWidget->count(); i++)
	{
		if (m_listWidget->item(i)->text() == fileName)
		{
			return;
		}
	}
	m_listWidget->addItem(fileName);
}

QStringList QMyListWidget::GetAllExePath()
{
	QStringList filePaths;
	int count = m_listWidget->count();
	for (int i = 0; i < count; i++)
	{
		filePaths.append(m_listWidget->item(i)->text());
	}
	return filePaths;
}

void QMyListWidget::ReadFile()
{
	QSettings setting("MySetting.ini", QSettings::IniFormat);
	setting.setIniCodec("UTF-8");
	m_listWidget->clear();
	setting.beginGroup(u8"快速启动路径列表:");
	QStringList groups = setting.childGroups();
	int num= setting.value(u8"数量", 0).toInt();
	for (int i = 0; i < groups.size(); i++)
	{
		if (i > num - 1)
		{
			break;
		}
		setting.beginGroup(groups[i]);
		QStringList keys = setting.childKeys();
		QString test = setting.value(u8"路径", "").toString();
		if (test != "")
		{
			m_listWidget->addItem(test);
		}
		setting.endGroup();
	}

	setting.endGroup();
}

void QMyListWidget::DeleteItem()
{
	QList<QListWidgetItem*> itmes = m_listWidget->selectedItems();

	for (int i = 0; i < itmes.size(); i++)
	{
		delete itmes[i];
	}
}

void QMyListWidget::SaveFile()
{
	QSettings setting("MySetting.ini", QSettings::IniFormat);
	setting.setIniCodec("UTF-8");
	int num = m_listWidget->count();
	setting.beginGroup(u8"快速启动路径列表:");
	setting.setValue(QString(u8"数量"), num);
	for (int i = 0; i < num; i++)
	{
		setting.beginGroup(QString(u8"路径%1:").arg(i + 1));
		setting.setValue(QString(u8"路径"), m_listWidget->item(i)->text());
		setting.endGroup();
	}
	setting.endGroup();
}
