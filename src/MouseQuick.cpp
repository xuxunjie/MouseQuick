#include "..\include\MouseQuick.h"
#include<Windows.h>
#pragma comment(lib,"user32.lib")
#include<qdebug.h>
#include<QSettings>

MouseQuick* MouseQuick::m_mouseQuick = NULL;
HHOOK MouseQuick::keyHook = NULL;//钩子对象
HHOOK MouseQuick::mouseHook = NULL;//钩子对象
//钩子过程函数
LRESULT  MouseQuick::keyBoardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	char szKey[256] = { 0 };
	//键盘按下
	if (nCode >= 0&& wParam==256)
	{
		PKBDLLHOOKSTRUCT hookStruct = (PKBDLLHOOKSTRUCT)lParam;
		qDebug() << "event:" << wParam<<"  key:"<< hookStruct->vkCode;
	}
	return CallNextHookEx(keyHook, nCode, wParam, lParam);//传给下一个钩子
}

//钩子过程函数
LRESULT  MouseQuick::MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	char szKey[256] = { 0 };
	//鼠标中键按下
	if (wParam== 519)
	{
		PMSLLHOOKSTRUCT hookStruct = (PMSLLHOOKSTRUCT)lParam;
		emit m_mouseQuick->ListenMouseWheelPress(QPointF(hookStruct->pt.x, hookStruct->pt.y));
		qDebug() << "event:" << wParam << "  x:" << hookStruct->pt.x;
		qDebug() << "y:" << hookStruct->pt.y << "  data:" << hookStruct->mouseData;
	}
	return CallNextHookEx(mouseHook, nCode, wParam, lParam);//传给下一个钩子
}

MouseQuick::MouseQuick(QWidget* parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_mouseQuick = this;
	StartAfterOpenComputer();
	InitUI();
	InstallHook();
	connect(this, SIGNAL(ListenMouseWheelPress(QPointF)), this, SLOT(ShowQuickStartWindow(QPointF)));
}


MouseQuick::~MouseQuick()
{
	UnistallHook();
	if (m_quickStartWidget != NULL)
	{
		m_quickStartWidget->close();
	}
}

void MouseQuick::StartAfterOpenComputer()
{
	QString appName = QApplication::applicationName();//程序名称

	QString appPath = QApplication::applicationFilePath();// 程序路径

	appPath = appPath.replace("/", "\\");

	QSettings* reg = new QSettings(
		"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
		QSettings::NativeFormat);

	QString val = reg->value(appName).toString();// 如果此键不存在，则返回的是空字符串
	if (val != appPath)
		reg->setValue(appName, appPath);// 如果移除的话，reg->remove(applicationName);

	reg->deleteLater();
}

void MouseQuick::ShowQuickStartWindow(QPointF pt)
{
	if (m_quickStartWidget!=NULL)
	{
		m_quickStartWidget->close();
	//	delete m_quickStartWidget;
	}
	QStringList filePaths = m_quickStartSettingWidget->GetAllExePath();
	m_quickStartWidget = new QMyQuickStartWidget();
	m_quickStartWidget->resize(400, 300);
	m_quickStartWidget->addButtons(filePaths);
	m_quickStartWidget->move(pt.x(), pt.y());
	m_quickStartWidget->setWindowTitle(u8"快速启动");
	m_quickStartWidget->setWindowFlags(Qt::WindowStaysOnTopHint| Qt::WindowCloseButtonHint);
	m_quickStartWidget->show();
}

void MouseQuick::InitUI()
{
	m_quickStartWidget = new QMyQuickStartWidget();
	m_quickStartWidget->resize(400, 300);
	m_quickStartSettingWidget = new QMyListWidget(this);
	ui.widget->AddTab(u8"快速启动路径配置", m_quickStartSettingWidget);
	ui.widget->AddTab("22", new QWidget);
	ui.widget->AddTab("33", new QWidget);
	ui.widget->AddTab("44", new QWidget);
}

void MouseQuick::InstallHook()
{
	keyHook = SetWindowsHookEx(
		WH_KEYBOARD_LL, //钩子类型
		keyBoardProc, //钩子处理函数
		NULL,
		0  //全局钩子
	);
	mouseHook = SetWindowsHookEx(
		WH_MOUSE_LL, //钩子类型
		MouseProc, //钩子处理函数
		NULL,
		0  //全局钩子
	);
}

void MouseQuick::UnistallHook()
{
	UnhookWindowsHookEx(keyHook);
	UnhookWindowsHookEx(mouseHook);
}