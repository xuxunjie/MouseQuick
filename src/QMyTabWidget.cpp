#include "..\include\QMyTabWidget.h"
#include<QHBoxLayout>
#include<qtabbar.h>
QMyTabWidget::QMyTabWidget(QWidget* parent)
	: QWidget(parent)
{
	InitUI();
}


QMyTabWidget::~QMyTabWidget()
{
	
}

void QMyTabWidget::AddTab(QString title, QWidget* widget)
{
	m_tabWidget->addTab(widget, title);
	m_tabBar->AddTab(title);
}

void QMyTabWidget::InitUI()
{
	m_tabWidget=new QTabWidget(this);
	m_tabWidget->tabBar()->setVisible(false);
	m_tabBar= new QMyTabBar(this);
	m_tabBar->setMinimumWidth(120);
	connect(m_tabBar, SIGNAL(CurrentIndexChange(int)), this, SLOT(SetTabIndex(int)));
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(m_tabBar);
	layout->addWidget(m_tabWidget);
	layout->setSpacing(0);
	layout->setStretch(0, 1);
	layout->setStretch(1, 10);
	layout->setContentsMargins(0, 0, 0, 0);
	setLayout(layout);
}

void QMyTabWidget::SetTabIndex(int index)
{
	m_tabWidget->setCurrentIndex(index);
}
