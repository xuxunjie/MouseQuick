#include "..\include\QMyTabBarButton.h"

QMyTabBarButton::QMyTabBarButton(int index, const QString& text, QWidget* parent)
	: QPushButton(text,parent)
{
	m_index = index;
	connect(this, SIGNAL(clicked()), this, SLOT(SendClickSignal()));
	if (m_index == 0)
	{
		SetSelectType(true);
	}
	else
	{
		SetSelectType(false);
	}
}

void QMyTabBarButton::SendClickSignal()
{
	emit ButtonClicked(m_index);
}

void QMyTabBarButton::SetSelectType(bool isClicked)
{
	m_isClicked = isClicked;
	if (m_isClicked)
	{
		this->setStyleSheet("QPushButton{ background-color: rgb(255, 255, 255);\n"
			"border:1px solid rgb(137, 140, 149);border-radius:5px;}");
	}
	else
	{
	this->setStyleSheet("QPushButton{ background-color: rgb(239, 239, 239)\n"
		"border:1px solid rgb(137, 140, 149); border-radius:5px;}");
	}
	repaint();
}

bool QMyTabBarButton::GetClickedType()
{
	return m_isClicked;
}
