#include"MyBaseUILib/include/QMyBaseUI.h"
#include"include/MouseQuick.h"
#include <QtWidgets/QApplication>
#include<QLabel>
#include<QMenuBar>
#include <QFile> 
#include <QTextStream> 
#include <QTime>
#include <QDebug>
#include <QtMsgHandler>
#include <QMessageLogContext>
#include <QMutex>
void LogMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    static QMutex mutex;
    mutex.lock();
    QString txt = "";
    switch (type)
    {
    case QtDebugMsg:
        txt = QString("Log: %1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
        break;
    }
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();
    QString strDate = date.toString("MM.dd");
    QString strTime = time.toString("hh:mm:ss");
    strDate.append(" ");
    strDate.append(strTime);
    QFile outFile("MyLog.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << strDate << " " << txt <<endl;
    outFile.flush();
    outFile.close();
    mutex.unlock();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qInstallMessageHandler(LogMessageHandler);
    QMyBaseUI *window=new QMyBaseUI(u8"MouseQuick V1.0.0");
    MouseQuick* widget = new MouseQuick(window);
    window->setCentralWidget(widget);
    window->show();
    QMenuBar* menuBar = window->menuBar();
   // menuBar->setStyleSheet("  QMenuBar{background-color: transparent;spacing: 3px; border-bottom:1px solid black;}");
    return a.exec();
}
