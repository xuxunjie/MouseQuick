/*****************************************************************//**

 *********************************************************************/
#pragma once

#include<QWidget>
#include<QPushButton>
#include<QResizeEvent>
#include"QMyTabBarButton.h"
class QMyTabBar :public QWidget
{
    Q_OBJECT

public:
    QMyTabBar(QWidget* parent = Q_NULLPTR);
    ~QMyTabBar();
    void AddTab(QString title);
protected:
    void resizeEvent(QResizeEvent* event) override;
signals:
    void CurrentIndexChange(int);
private slots:
    void SetButtonClicked(int index);
private:
    QWidget* m_tabBar;
    QList<QMyTabBarButton*>m_titles;
};
