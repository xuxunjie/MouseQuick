/*****************************************************************//**
 * QMyTabWidget
 * 自定义tabWidget
 * 修改了tabbar的样式
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once
#include"QMyTabBar.h"
#include<QTabWidget>

class QMyTabWidget :public QWidget
{
    Q_OBJECT

public:
    QMyTabWidget(QWidget* parent = Q_NULLPTR);
    ~QMyTabWidget();
    void AddTab(QString title, QWidget* widget);
private:
    void InitUI();
private slots:
    void SetTabIndex(int index);
private:
    QTabWidget* m_tabWidget;
    QMyTabBar* m_tabBar;
};
