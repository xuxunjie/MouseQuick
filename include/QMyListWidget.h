/*****************************************************************//**
 * QMyListWidget
 * 自定义ListWidget
 * 具备以下功能 
 * 添加按钮 删除按钮 保存按钮 
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once

#include<QListWidget>
#include<QPushButton>

class QMyListWidget :public QWidget
{
    Q_OBJECT

public:
    QMyListWidget(QWidget* parent = nullptr);
    QStringList GetAllExePath();//返回所有应用路径
    //读取配置文件
    void ReadFile();
private slots:
    void AddItem();
    void DeleteItem();
    //保存配置文件
    void SaveFile();   
signals:
    void RefreshData();//数据发生变动需要重新加载
private:
    void InitUI();
private:
    QListWidget* m_listWidget;
    QPushButton* m_addButton;
    QPushButton* m_deleteButton;
    QPushButton* m_saveButton;
};
