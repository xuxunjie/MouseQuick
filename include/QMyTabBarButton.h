/*****************************************************************//**
 
 *********************************************************************/
#pragma once

#include<QPushButton>

class QMyTabBarButton :public QPushButton
{
    Q_OBJECT

public:
    QMyTabBarButton(int index,const QString& text, QWidget* parent = nullptr);
    void SetSelectType(bool isClicked);
    bool GetClickedType();
private slots:
    void SendClickSignal();
signals:
    void ButtonClicked(int);
private:
    int m_index;//按钮序号
    bool m_isClicked;//是否被选中
};
