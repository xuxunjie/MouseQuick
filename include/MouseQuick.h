/*****************************************************************//**
 * \file   MyQScript.h
 * \brief  脚本类 用于调用处理数据
 * 
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once

#include<QWidget>
#include "ui_MouseQuick.h"
#include<Windows.h>
#pragma comment(lib,"user32.lib")
#include"QMyListWidget.h"
#include"QMyQuickStartWidget.h"

class MouseQuick :public QWidget
{
    Q_OBJECT

public:
    MouseQuick(QWidget* parent = Q_NULLPTR);
    ~MouseQuick();
    
    //开机自启动
    void StartAfterOpenComputer();
private slots:
    //显示快速启动弹窗
    void ShowQuickStartWindow(QPointF pt);
signals:
    //检测到鼠标滚轮按下
    void ListenMouseWheelPress(QPointF pt);
private:
    void InitUI();
    //创建并释放钩子函数
    static void InstallHook();
    static void UnistallHook();
    //钩子过程函数
    static LRESULT CALLBACK keyBoardProc(int nCode, WPARAM wParam, LPARAM lParam);
    static LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);
private:
    Ui::MouseQuickClass ui;
    static MouseQuick* m_mouseQuick;//定义一个静态ToolA类
    static HHOOK keyHook;//钩子对象
    static HHOOK mouseHook;//钩子对象
    QMyQuickStartWidget* m_quickStartWidget;//快速启动弹窗
    QMyListWidget* m_quickStartSettingWidget;//快速启动的配置窗口
};
