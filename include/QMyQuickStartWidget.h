/*****************************************************************//**
 * QMyQuickStartWidget
 * 自定义快速启动弹窗
 * 具备以下功能
 * 显示一系列按钮 并启动
 *
 * \author Administrator
 * \date   March 2022
 *********************************************************************/
#pragma once

#include<QListWidget>
class QMyQuickStartWidget :public QWidget
{
	Q_OBJECT

public:
	QMyQuickStartWidget(QWidget* parent = nullptr);
	//添加对应路径的按钮
	void addButtons(QStringList paths);
	void addButton(QString path);
private slots:
	//启动应用
	void OpenEXE(QListWidgetItem* item);
signals:

private:
	void InitUI();
private:
	QListWidget* m_listWidget;
};
